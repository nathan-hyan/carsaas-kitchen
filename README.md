# Carsaas Kitchen - The right meal for you.

Landing page dedicated to food and culinary arts. built using **HTML**, **SCSS** and **JavaScript.**


## Installation

1. Just run index.html.

## Contact

 - Email: [exequiel@hyan.dev](mailto:exequiel@hyan.dev)
 - Website: https://hyan.dev
 - LinkedIn: [https://www.linkedin.com/in/exequielm2048/](https://www.linkedin.com/in/exequielm2048/)
