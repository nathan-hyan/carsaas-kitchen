// Dynamic nav bar

var scrollEffect = document.getElementById("navigational");

window.onscroll = function () {
  if (window.pageYOffset > 500) {
    scrollEffect.style.background = "#333";
  } else {
    scrollEffect.style.background = "transparent";
  }
};
